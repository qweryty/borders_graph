#include<SDL2/SDL.h>
#include<iostream>
//Screen dimension constants
const int SCREEN_WIDTH = 256;
const int SCREEN_HEIGHT = 256;

using namespace std;

int main(){
	//The window we'll be rendering to
    SDL_Window* window = NULL;
    
    //The surface contained by the window
    SDL_Surface* screenSurface = NULL;

    //Initialize SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    }

		else
    {
        //Create window
				SDL_Window *window;
				SDL_Renderer* renderer;
				SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, 0, &window, &renderer);
        if( window == NULL )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        }
				else
        {
						SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
						SDL_RenderDrawLine(renderer, 128, 0, 128, 256);
						SDL_RenderDrawLine(renderer, 0, 127, 256, 127);

						SDL_SetRenderDrawColor(renderer, 255, 0, 0, 0);
						for(int x = -128; x < 128; ++x){
							for(int y = -128; y < 128; ++y){
								char bx = x;
								char by = y;
								//CHANGE THIS
								if(bx >= (char)(by * 2 + 2))
									SDL_RenderDrawPoint(renderer, x + 128, 127 - y);
							}
						}
            //Update the renderer
						SDL_RenderPresent(renderer);

            //Wait two seconds
        }
    }

		cin.get();
		//Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    SDL_Quit();

    return 0;
}
